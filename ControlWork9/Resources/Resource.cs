﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork9.Resources
{
  public class Resource
  {

    public class ErrorResurces : Resource
    {
    }

    public class MessageResurces : Resource
    {
    }

    public class SuccessResurces : Resource
    {
    }

    public class InputResurces : Resource
    {
    }
  }
}
