﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ControlWork9.Data;
using ControlWork9.Data.Entities;
using ControlWork9.Data.IRepositories;
using ControlWork9.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using ControlWork9.Models;
using ControlWork9.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ControlWork9.Controllers
{
 

  public class HomeController : Controller
  {
    private readonly ApplicationDbContext _db;
    private readonly UserManager<User> _UserManager;
    private readonly ITransactionHistoryRepository _repository;

    public HomeController(
      ITransactionHistoryRepository repository,
      ApplicationDbContext db,
      UserManager<User> userManager)
    {
      _db = db;
      _UserManager = userManager;
      _repository = repository;
    }

    public IActionResult Index()
    {
      return View();
    }


    public async Task<IActionResult> PaymentHistory(
      DateTime? dateFrom,
      DateTime? dateTo)
    {
      // var allEntities = _repository.GetList();
      // // var all = allEntities.IncludeSingle<TransactionHistory, User>(b => b.RecipientUser, b => b.SenderUser);
      // IEnumerable<TransactionHistory> list = allEntities.Where(c =>
      //   c.RecipientUserId.Equals(_UserManager.GetUserId(User)) 
      //   || c.SenderUserId.Equals(_UserManager.GetUserId(User)));
      IQueryable<TransactionHistory> histories = _db.TransactionHistories.Where(c =>
          c.RecipientUserId.Equals(_UserManager.GetUserId(User))
          || c.SenderUserId.Equals(_UserManager.GetUserId(User)))
        .Include(c => c.RecipientUser)
        .Include(c => c.SenderUser);

      if (dateFrom.HasValue)
      {
        histories = histories.Where(s => s.DateTime >= dateFrom.Value);
      }

      if (dateTo.HasValue)
      {
        histories = histories.Where(s => s.DateTime <= dateTo.Value);
      }

      return View(histories.ToList());
    }


    public IActionResult About()
    {
      ViewData["Message"] = "Your application description page.";

      return View();
    }

    public IActionResult Contact()
    {
      ViewData["Message"] = "Your contact page.";

      return View();
    }

    [HttpGet]
    [AllowAnonymous]
    public IActionResult Payment()
    {
      return View();
    }

    [HttpGet]
    [AllowAnonymous]
    public IActionResult PaymentFromUser()
    {
      return View();
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> PaymentFromUser(UserPaymentViewModel model)
    {
      if (ModelState.IsValid)
      {
        User recipientUser = await _db.Users.FirstOrDefaultAsync(x =>
          x.AccountNumber == Int32.Parse(model.AccountNumber));
        //есть ли такой номер счета 
        if (recipientUser == null)
        {
          ViewBag.Error = $" not found account number {model.AccountNumber} try again";
          return View(model);
        }

        //доступен ли баланс 
        User sender = await _db.Users.FirstOrDefaultAsync(x =>
          x.AccountNumber == Int32.Parse(_UserManager.GetUserName(User)));

        if (sender.Balance < model.Value || sender.Balance <= 0)
        {
          ViewBag.Error = $" Your Balance {sender.Balance} KGS not match to pay {model.Value}";
          return View(model);
        }

        //перевод самому себе 
        if (sender.AccountNumber == Int32.Parse(model.AccountNumber))
        {
          ViewBag.Error = $" You can`t pay on your account {model.AccountNumber} try pay another account";
          return View(model);
        }

        //записываем транзакцию 
        TransactionHistory transaction = new TransactionHistory
        {
          Value = model.Value,
          SenderUser = sender,
          After = recipientUser.Balance + model.Value,
          Befor = recipientUser.Balance,
          SenderUserId = sender.Id,
          DateTime = DateTime.Now,
          RecipientUser = recipientUser,
          RecipientUserId = recipientUser.Id,
          Status = "success"
        };

        //обновляем данные пользователей 
        sender.Balance -= model.Value;
        recipientUser.Balance += model.Value;

        //_db.TransactionHistories.Add(transaction);
        _repository.Create(transaction);
        _db.Users.Update(sender);
        _db.Users.Update(recipientUser);
        await _db.SaveChangesAsync();
        ViewBag.Message = $"Pay successfuly to {model.AccountNumber} {model.Value} KGS";
        return View();
      }

      return View(model);
    }

    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Payment(AnonimPaymentViewModel model)
    {
      if (ModelState.IsValid)
      {
        User user = await _db.Users.FirstOrDefaultAsync(x => x.AccountNumber == Int32.Parse(model.AccountNumber));
        if (user == null)
        {
          ViewBag.Error = $"invalid Account Number {model.AccountNumber}";
          return View(model);
        }

        //данный пользователь anonimus
        User anonimus = await _db.Users.FirstOrDefaultAsync(x => x.AccountNumber == 100001);
        TransactionHistory transaction = new TransactionHistory
        {
          Value = model.Value,
          SenderUser = anonimus,
          After = user.Balance + model.Value,
          Befor = user.Balance,
          SenderUserId = anonimus.Id,
          DateTime = DateTime.Now,
          RecipientUser = user,
          RecipientUserId = user.Id,
          Status = "success"
        };

        user.Balance += model.Value;
        _db.TransactionHistories.Add(transaction);
        _db.Users.Update(user);
        await _db.SaveChangesAsync();
        ViewBag.Message = $"Pay successfuly to {model.AccountNumber} {model.Value} KGS";
        return View();
      }

      return View(model);
    }

    public IActionResult Privacy()
    {
      return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }

    [Authorize]
    public async Task<String> ShowBalance()
    {
      User user = await _db.Users.FirstOrDefaultAsync(x =>
        x.AccountNumber == Int32.Parse(_UserManager.GetUserName(User)));
      if (user == null)
      {
        return "";
      }

      return $"Balance: {user.Balance} KGS";
    }
  }
}