﻿using System;
using System.Collections.Generic;
using System.Text;
using ControlWork9.Data.Configurations;
using ControlWork9.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ControlWork9.Data
{
  public class ApplicationDbContext : IdentityDbContext
  {
    //Таблицы
    public DbSet<User> Users { get; set; }
    public DbSet<TransactionHistory> TransactionHistories { get; set; }
    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      builder.ApplyConfiguration(new TransactionHistoryConfiguration());
    }
  }
}
