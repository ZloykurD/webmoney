﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork9.Data.Entities;
using ControlWork9.Data.Repositories;
using ControlWork9.IRepositories;

namespace ControlWork9.Data.IRepositories
{
 public interface ITransactionHistoryRepository : IRepository<TransactionHistory>
  {
  }
}