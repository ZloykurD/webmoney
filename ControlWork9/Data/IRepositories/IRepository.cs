﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork9.Data.Entities;

namespace ControlWork9.IRepositories
{
 public interface IRepository<T> where T : Entity
  {
   IQueryable<T> GetList();
    void Create(T item);
    void Update(T item);
    void Delete(T item);
    Task<T> Find(String id);
  }
}