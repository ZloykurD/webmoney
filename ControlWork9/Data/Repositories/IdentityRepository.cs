﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ControlWork9.Data.Repositories
{
  public interface IIdentityRepository<T> where T : IdentityUser
  {
  }

  public class IdentityRepository<T> : IIdentityRepository<T> where T : IdentityUser
  {
  }


}