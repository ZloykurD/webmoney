﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork9.Data.Entities;
using ControlWork9.Data.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace ControlWork9.Data.Repositories
{
  public class TransactionHistoryRepository : Repository<TransactionHistory>, ITransactionHistoryRepository
  {
    public TransactionHistoryRepository(ApplicationDbContext context) : base(context)
    {
      All = context.TransactionHistories;
    }
    
  }
}