﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork9.Data.Entities
{
  public class TransactionHistory : Entity
  {
    //сумма
    public double Value { get; set; }
    //баланс до
    public double Befor { get; set; }
    //баланс после
    public double After { get; set; }
    //получатель
    public String RecipientUserId { get; set; }
    public User RecipientUser { get; set; }
    //отправитель
    public String SenderUserId { get; set; }
    public User SenderUser { get; set; }

    //дата и время транзакции
    public DateTime DateTime { get; set; }
    //статус
    public String Status { get; set; }
    
  }
}