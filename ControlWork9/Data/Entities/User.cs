﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ControlWork9.Data.Entities
{
  public class User : IdentityUser
  {
    public String Name { get; set; }
    public int AccountNumber { get; set; }
    public double Balance { get; set; }

    public IEnumerable<TransactionHistory> Serders { get; set; }
    public IEnumerable<TransactionHistory> Recipients { get; set; }
  }
}
