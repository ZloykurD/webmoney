﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork9.Data.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace ControlWork9.Data
{
  public class ApplicationDbInitializer
  {
    public async Task SeedAsync(IApplicationBuilder app)
    {
      using (var score = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
      {
        var userManager = score.ServiceProvider.GetRequiredService<UserManager<User>>();
        var roleManager = score.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

        if (!await roleManager.RoleExistsAsync("Admin"))
        {
          IdentityRole roleAdmin = new IdentityRole("Admin");
          await roleManager.CreateAsync(roleAdmin);
        }

        if (!await roleManager.RoleExistsAsync("User"))
        {
          IdentityRole roleUser = new IdentityRole("User");
          await roleManager.CreateAsync(roleUser);
        }

        User admin = await userManager.FindByNameAsync("100000");
        if (admin == null)
        {
          var user = new User
          {
            UserName = "100000",
            Email = "admin@webmoney.kg",
            Name = "Admin",
            Balance = 0,
            AccountNumber = 100000
          };
          var result = await userManager.CreateAsync(user, "Admin321321");
          if (result.Succeeded)
          {
            await userManager.AddToRoleAsync(user, "Admin");
          }
        }

        User anonimus = await userManager.FindByNameAsync("100001");
        if (anonimus == null)
        {
          var user = new User
          {
            UserName = "100001",
            Email = "anonimus@webmoney.kg",
            Name = "Anonimus",
            Balance = 0,
            AccountNumber = 100001
          };
          var result = await userManager.CreateAsync(user, "Admin321321");
          if (result.Succeeded)
          {
            await userManager.AddToRoleAsync(user, "User");
          }
        }
      }
    }
  }
}