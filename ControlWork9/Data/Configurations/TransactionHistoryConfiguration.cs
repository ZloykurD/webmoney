﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork9.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ControlWork9.Data.Configurations
{
  public class TransactionHistoryConfiguration : IEntityTypeConfiguration<TransactionHistory>
  {
    public void Configure(EntityTypeBuilder<TransactionHistory> builder)
    {
      builder
        .HasOne(c => c.SenderUser)
        .WithMany(c => c.Recipients)
        .HasForeignKey(c => c.SenderUserId)
        .OnDelete(DeleteBehavior.Restrict);

      builder
        .HasOne(c => c.RecipientUser)
        .WithMany(c => c.Serders)
        .HasForeignKey(c => c.RecipientUserId)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}