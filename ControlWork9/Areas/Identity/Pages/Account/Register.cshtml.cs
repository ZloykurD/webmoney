﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using ControlWork9.Data;
using ControlWork9.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace ControlWork9.Areas.Identity.Pages.Account
{
  [AllowAnonymous]
  public class RegisterModel : PageModel
  {
    private readonly SignInManager<User> _signInManager;
    private readonly UserManager<User> _userManager;
    private readonly ILogger<RegisterModel> _logger;
    private readonly ApplicationDbContext _db;

    public RegisterModel(
      ApplicationDbContext db,
      UserManager<User> userManager,
      SignInManager<User> signInManager,
      ILogger<RegisterModel> logger)
    {
      _userManager = userManager;
      _signInManager = signInManager;
      _logger = logger;
      _db = db;
    }

    [BindProperty] public InputModel Input { get; set; }

    public string ReturnUrl { get; set; }

    public class InputModel
    {
      [Required]
      [EmailAddress]
      [Display(Name = "Email")]
      public string Email { get; set; }

      [Required] 
      [Display(Name = "Name")] public string Name { get; set; }
      
      [Required]
      [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.",
        MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = "Password")]
      public string Password { get; set; }

      [DataType(DataType.Password)]
      [Display(Name = "Confirm password")]
      [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
      public string ConfirmPassword { get; set; }
    }

    public void OnGet(string returnUrl = null)
    {
      ReturnUrl = returnUrl;
    }

    public async Task<IActionResult> OnPostAsync(string returnUrl = null)
    {
      returnUrl = returnUrl ?? Url.Content("~/");
      if (ModelState.IsValid)
      {
        
        int AccountNumber = await GenerateAccountNumber();

        var user = new User
        {
          UserName = AccountNumber.ToString(),
          AccountNumber = AccountNumber,
          Name = Input.Name,
          Balance = 0,
          Email = Input.Email
        };

        var result = await _userManager.CreateAsync(user, Input.Password);
        if (result.Succeeded)
        {
          _logger.LogInformation("User created a new account with password.");
          await _userManager.AddToRoleAsync(user, "User");
          await _signInManager.SignInAsync(user, isPersistent: false);
          return LocalRedirect(returnUrl);
        }

        foreach (var error in result.Errors)
        {
          ModelState.AddModelError(string.Empty, error.Description);
        }
      }

      // If we got this far, something failed, redisplay form
      return Page();
    }

    private async Task<int> GenerateAccountNumber()
    {
      Random random = new Random();
      int number = random.Next(100000,999999);//fixed

      User user = _db.Users.FirstOrDefault(x => x.AccountNumber == number);
      if (user != null)
      {
        number = await GenerateAccountNumber();
      }
    
      return number;
    }
  }
}