﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork9.ViewModels
{
  public class AnonimPaymentViewModel
  {
    public String AccountNumber { get; set; }
    public double Value { get; set; }
  }
}
